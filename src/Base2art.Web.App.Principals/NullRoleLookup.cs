﻿namespace Base2art.Web.App.Principals
{
    using System.Security.Claims;
    using System.Threading.Tasks;

    public class NullRoleLookup : IRoleLookup
    {
        public Task<string[]> GetRolesAsync(ClaimsPrincipal principal) => Task.FromResult(new string[0]);
    }
}