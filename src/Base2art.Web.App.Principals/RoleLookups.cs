﻿namespace Base2art.Web.App.Principals
{
    using System;
    using System.Collections.Generic;
    using System.Security.Claims;
    using System.Threading.Tasks;

    public static class RoleLookups
    {
        public static async Task<HashSet<string>> GetRolesSetAsync(this IRoleLookup lookup, ClaimsPrincipal principal)
        {
            if (lookup == null)
            {
                throw new ArgumentNullException(nameof(lookup));
            }

            var roles = await lookup.GetRolesAsync(principal);
            return new HashSet<string>(roles ?? new string[0], StringComparer.OrdinalIgnoreCase);
        }
    }
}