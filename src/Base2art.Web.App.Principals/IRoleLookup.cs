﻿namespace Base2art.Web.App.Principals
{
    using System.Security.Claims;
    using System.Threading.Tasks;

    public interface IRoleLookup
    {
        Task<string[]> GetRolesAsync(ClaimsPrincipal principal);
    }
}